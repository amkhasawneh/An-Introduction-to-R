x <- rnorm(50)
y <- rnorm(x)

plot(x,y)

ls()
rm(x,y)
ls()

x <- 1:20
w <- 1 + sqrt(x)/2

dummy <- data.frame(x=x, y=x + rnorm(x)*w)
dummy

fm <- lm(y ~ x, data = dummy)
summary(fm)

fm1 <- lm(y ~ x, data=dummy, weight=1/w^2)
summary(fm1)

attach(dummy)
lrf <- lowess(x,y)
plot(x,y)

lines(x,lrf$y)

abline(0,1,lty=3)
abline(coef(fm))
abline(coef(fm1), col="red")
detach()

plot(fitted(fm), resid(fm),
xlab="Fitted values",
ylab="Residuals",
main="Residuals vs Fitted")

qqnorm(resid(fm), main="Residuals Rankit Plot")

rm(fm, fm1, lrf, x, dummy)


x <- seq(-pi, pi, len=50)
y <- x
f <- outer(x, y, function(x, y) cos(y)/(1 + x^2))
oldpar <- par(no.readonly = TRUE)
par(pty="s")
contour(x, y, f)
contour(x, y, f, nlevels=15, add=TRUE)
fa <- (f-t(f))/2
contour(x, y, fa, nlevels=15)
par(oldpar)
image(x, y, f)
image(x, y, fa)
objects(); rm(x, y, f, fa)




th <- seq(-pi, pi, len=100)
z <- exp(1i*th)
par(pty="s")
plot(z, type="l")
w <- rnorm(100) + rnorm(100)*1i
w <- ifelse(Mod(w) > 1, 1/w, w)
plot(w, xlim=c(-1,1), ylim=c(-1,1), pch="+",xlab="x", ylab="y")
lines(z)

w <- sqrt(runif(100))*exp(2*pi*runif(100)*1i)
plot(w, xlim=c(-1,1), ylim=c(-1,1), pch="+", xlab="x", ylab="y")
lines(z)

rm(th, w, z)


q()

y


help(sink)
?sink
ls()
objects()

c(10.4, 5.6, 3.1, 6.4, 21.7) -> x
y <- c(x, 0, x)
v <- 2*x + y + 1
range(x)
length(x)
sum(x)
prod(x)
mean(x)
var(x)
sort(x)
order(x)
sort.list(x)
pmax(x, y)
pmin(x,y)
sqrt(-17)
sqrt(-17+0i)
seq(-5, 5, by=.2) -> s3
s4 <- seq(length=51, from=-5, by=.2)
s5 <- rep(x, times=5)
s6 <- rep(x, each=5)

temp <- x > 13

z <- c(1:3,NA)
ind <- is.na(z)

0/0
Inf - Inf

?Inf


?Quotes
labs <- paste(c("X","Y"), 1:10, sep=" ", collapse = "||")
labs
?paste

y <- x[!is.na(x)]
y

(x+1)[(!is.na(x)) & x>0] -> z
z

c("x","y")[rep(c(1,2,2,1), times=4)]
y <- x[-(1:5)]


fruit <- c(5, 10, 1, 20)
names(fruit) <- c("orange", "banana", "apple", "peach")
lunch <- fruit[c("apple","orange")]
lunch


x[is.na(x)] <- 0
#replaces any missing values in x by zeros and

y[y < 0] <- -y[y < 0]
#has the same effect as
y <- abs(y)


g <- c("I", "am", "me.")
g
typeof(g) 
mode(g)
attributes(g)


z <- 1:10
digits <- as.character(z)
d <- as.integer(digits)


e <- numeric()
e[3] <- 17
v <- character()
mode(v)

alpha <- 1:12
alpha <- alpha[2 * 1:5]

attr(z, "dim") <- c(10,10) -> p
attributes(alpha)


state <- c("tas", "sa", "qld", "nsw", "nsw", "nt", "wa", "wa",
           "qld", "vic", "nsw", "vic", "qld", "qld", "sa", "tas",
           "sa", "nt", "wa", "vic", "qld", "nsw", "nsw", "wa",
           "sa", "act", "nsw", "vic", "vic", "act")
statef <- factor(state)
statef
levels(statef)
incomes <- c(60, 49, 40, 61, 64, 60, 59, 54, 62, 69, 70, 42, 56,
             61, 61, 61, 58, 51, 48, 65, 49, 49, 41, 48, 52, 46,
             59, 46, 58, 43)

incmeans <- tapply(incomes, statef, mean)
incmeans
?tapply
tapply(incomes, statef)

stdError <- function(x) sqrt(var(x)/length(x))

incster <- tapply(incomes, statef, stdError)
incster
rm(z)

dim(z) <- c(3,5,100)


x <- array(1:20, dim=c(4,5)) # Generate a 4 by 5 array.
x
i <- array(c(1:3,3:1), dim=c(3,2))
i
x[i]
x[i] <- 0
x
Xb <- matrix(0, n, b)
Xv <- matrix(0, n, v)
ib <- cbind(1:n, blocks)
iv <- cbind(1:n, varieties)
Xb[ib] <- 1
Xv[iv] <- 1
X <- cbind(Xb, Xv)
N <- crossprod(Xb, Xv)
N <- table(blocks, varieties)


z <- array(0, c(3,4,2))
z
dim(z)


d <- outer(0:9, 0:9)
fr <- table(outer(d, d, "-"))
plot(fr, xlab="Determinant", ylab="Frequency")
fr


?aperm()


x  <- array(1:24, 2:4)
xt <- aperm(x, c(2,1,3))
x
xt


g <- matrix(data = 1:16, nrow = 4, ncol = 4, byrow = T)
g
diag(g)
diag(4) #4x4 identity matrix

cbind(1, s3, s4)
?tapply
statef
statefr <- table(statef)
statefr
tapply(statef, statef, length)
?length


factor(cut(incomes, breaks = 35+10*(0:7))) -> incomef
incomef
table(incomef,statef)

accountants <- data.frame(home=statef, loot=incomes, shot=incomef)

search()
?autoload


data("ChickWeight")
ChickNew <- edit(ChickWeight)

xnew <- edit(data.frame())

library(stats)


twosam <- function(y1, y2) {
  n1 <- length(y1); n2 <- length(y2)
  yb1 <- mean(y1); yb2 <- mean(y2)
  s1 <- var(y1); s2 <- var(y2)
  s <- ((n1-1)*s1 + (n2-1)*s2)/(n1+n2-2)
  tst <- (yb1 - yb2)/sqrt(s*(1/n1 + 1/n2))
  tst
}



open.account <- function(total) {
  list(
    deposit = function(amount) {
      if(amount <= 0)
        stop("Deposits must be positive!\n")
      total <<- total + amount
      cat(amount, "deposited. Your balance is", total, "\n\n")
    },
    withdraw = function(amount) {
      if(amount > total)
        stop("You don't have that much money!\n")
      total <<- total - amount
      cat(amount, "withdrawn. Your balance is", total, "\n\n")
    },
    balance = function() {
      cat("Your balance is", total, "\n\n")
    }
  )
}
ross <- open.account(100)
robert <- open.account(200)
ross$withdraw(30)
ross$balance()
robert$balance()
ross$deposit(50)
ross$balance()
ross$withdraw(500)


cat('no')


methods(class = "data.frame")
methods(plot)



kalythos <- data.frame(x = c(20,35,45,55,70), n = rep(50,5),
                       y = c(6,17,26,37,44))
kalythos$Ymat <- cbind(kalythos$y, kalythos$n - kalythos$y)

fmp <- glm(Ymat ~ x, family = binomial(link=probit), data = kalythos)
fml <- glm(Ymat ~ x, family = binomial, data = kalythos)
summary(fmp)
summary(fml)
ld50 <- function(b) -b[1]/b[2]
ldp <- ld50(coef(fmp)); ldl <- ld50(coef(fml)); c(ldp, ldl)



x <- c(0.02, 0.02, 0.06, 0.06, 0.11, 0.11, 0.22, 0.22, 0.56, 0.56,
              1.10, 1.10)
y <- c(76, 47, 97, 107, 123, 139, 159, 152, 191, 201, 207, 200)
fn <- function(p) sum((y - (p[1] * x)/(p[2] + x))^2)
plot(x, y)
xfit <- seq(.02, 1.1, .05)
yfit <- 200 * xfit/(0.1 + xfit)
lines(spline(xfit, yfit))
out <- nlm(fn, p = c(200, 0.1), hessian = TRUE)
out
sqrt(diag(2*out$minimum/(length(y) - 2) * solve(out$hessian)))

plot(x, y)
xfit <- seq(.02, 1.1, .05)
yfit <- 212.68384222 * xfit/(0.06412146 + xfit)
lines(spline(xfit, yfit))
df <- data.frame(x=x, y=y)
fit <- nls(y ~ SSmicmen(x, Vm, K), df)
fit
summary(fit)



x <- c(1.6907, 1.7242, 1.7552, 1.7842, 1.8113,
       1.8369, 1.8610, 1.8839)
y <- c( 6, 13, 18, 28, 52, 53, 61, 60)
n <- c(59, 60, 62, 56, 63, 59, 62, 60)
fn <- function(p)
  sum( - (y*(p[1]+p[2]*x) - n*log(1+exp(p[1]+p[2]*x))
          + log(choose(n, y)) ))
out <- nlm(fn, p = c(-50,20), hessian = TRUE)
sqrt(diag(solve(out$hessian)))


library(ggplot2)


pairs(df)


coplot(x, y, n)

qqplot(x, y)

qplot(x, y)

hist(x, probability = T)

dotchart(x)

image(x, y, n)
contour(x, y, n)
persp(x, y, n)

polygon(x, y)

demo(Japanese)

locator()

legend(locator(1), as.character(0:25), pch = 0:25)


list.files
system
shell
Sys.which
shQuote
